## Installation

First install the [`devtools`](https://rawgit.com/rstudio/cheatsheets/master/package-development.pdf) package and load it:

```R
install.packages("devtools")
library(devtools)
```

Then, you can install `clewsbiosphere` by directly loading it from the PIK gitlab server:

```R
devtools::install_git("git@gitlab.pik-potsdam.de:stenzel/clews_biosphere.git")
library(clewsbiosphere)
```

## Examples
